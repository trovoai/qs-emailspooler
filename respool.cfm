<cfset undeliverDir = "c:\ColdFusion11\cfusion\Mail\Undelivr">
<cfset spoolDir = "c:\ColdFusion11\cfusion\Mail\Spool">
<cfset findText = "alerts@trovo.ai">
<cfset replaceText = "alerts@leasestation.com">

<cfdirectory directory="#undeliverDir#" action="list" name="fileList" type="file" />

<cfloop query="fileList">
file Processing - <cfoutput>#fileList.Name#</cfoutput>

    <cfset filepath = "#fileList.Directory#\#fileList.Name#">
    <cfset destpath = "#spoolDir#\#fileList.Name#">
    <cffile action="read" file="#filepath#" variable="mailBody" >

    <cfif findnocase("lease_batch@timepaymentcorp.com",mailBody,0)>
        - Ignored<br>
    <cfelse>
        <cfset mailBody = replaceNoCase(mailBody,findText,replaceText,'ALL')>
        <cffile action="write" file="#destpath#" output="#mailBody#" addNewline="yes">
        <cffile action="delete" file="#filepath#">
        - Spooled<br>
    </cfif>
</cfloop>